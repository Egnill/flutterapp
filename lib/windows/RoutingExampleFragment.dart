import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoutingExampleFragment extends StatelessWidget {
  RoutingExampleFragment({Key key, this.title}) : super(key: key);

  String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
    );
  }
}