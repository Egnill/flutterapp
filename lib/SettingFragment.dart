import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SettingFragment extends StatefulWidget {
  SettingFragment({Key key, this.title}) : super(key: key);

  String title;

  @override
  State<StatefulWidget> createState() => _SettingFragmentPage();
}

class _SettingFragmentPage extends State<SettingFragment>
    with TickerProviderStateMixin {
  AnimationController animController;
  CurvedAnimation curvedAnim;

  @override
  void initState() {
    super.initState();
    animController = AnimationController(
        duration: const Duration(milliseconds: 3000),
        vsync: this);
    curvedAnim = CurvedAnimation(
        parent: animController,
        curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FadeTransition(
          opacity: curvedAnim,
          child: FlutterLogo(
            size: 200,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Fade',
        onPressed: () {
          animController.forward();
        },
        child: Icon(Icons.brush),
      ),
    );
  }
}
