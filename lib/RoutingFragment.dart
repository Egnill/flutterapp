import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/windows/RoutingExampleFragment.dart';

class RoutingFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _RoutingPage(title: 'Routing'),
      routes: _setRoutes(),
    );
  }

  Map<String, WidgetBuilder> _setRoutes() {
    return {
      '/a': (BuildContext contex) => RoutingExampleFragment(title: 'Click A'),
      '/b': (BuildContext contex) => RoutingExampleFragment(title: 'Click B'),
      '/c': (BuildContext contex) => RoutingExampleFragment(title: 'Click C')
    };
  }
}

class _RoutingPage extends StatelessWidget {
  _RoutingPage({Key key, this.title}) : super(key: key);

  String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed('/a');
              },
              child: Text('Button A'),
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed('/b');
              },
              child: Text('Button B'),
            ),
            const SizedBox(height: 30),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed('/c');
              },
              child: Text('Button C'),
            ),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

}