import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class _Counter {
  int _count;

  int get count => _count;

  final BehaviorSubject<int> onCountUpd;
  _Counter(this._count) : this.onCountUpd = BehaviorSubject<int>.seeded(_count);

  Future incrementCount() async {
    onCountUpd.add(++_count);
  }
}

final _count = _Counter(0);

class CounterFragment extends StatelessWidget {
  CounterFragment({Key key, this.title}) : super(key: key);

  var title;

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text(title),
    ),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          StreamBuilder<int>(
            stream: _count.onCountUpd,
            builder: (context, snapshot) => Text(
                'You have pushed the button ${snapshot.data} times:'
            ),
          ),
          StreamBuilder(
            stream: _count.onCountUpd,
            builder: (context, snapshot) => Text(
              '${snapshot.data}',
              style: Theme.of(context).textTheme.display1,
            ),
          ),
        ],
      ),
    ),
    floatingActionButton: FloatingActionButton(
      onPressed: _count.incrementCount,
      child: Icon(Icons.add),
    ),
  );
}