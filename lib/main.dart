import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/CallFragment.dart';
import 'package:flutter_app/CanvasFragment.dart';
import 'package:flutter_app/CounterFragment.dart';
import 'package:flutter_app/IntentFragment.dart';
import 'package:flutter_app/JsonFragment.dart';
import 'package:flutter_app/LongTaskFragment.dart';
import 'package:flutter_app/RoutingFragment.dart';
import 'package:flutter_app/SettingFragment.dart';

import 'CallFragment.dart';
import 'CanvasFragment.dart';
import 'RoutingFragment.dart';
import 'SecondFragment.dart';
import 'SettingFragment.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: _routingApp(),
    );
  }

  Map<String, WidgetBuilder> _routingApp() {
    return {
      'messages': (BuildContext context) => SecondFragment(title: "Messages"),
      'call': (BuildContext context) => CallFragment(title: "Call"),
      'setting': (BuildContext context) => SettingFragment(title: "Setting"),
      'canvas': (BuildContext context) => CanvasFragment(),
      'rout': (BuildContext context) => RoutingFragment(),
      'intent': (BuildContext context) => IntentFragment(title: "Intent"),
      'json': (BuildContext context) => JsonFragment(title: "Json"),
      'longtask': (BuildContext context) => LongTaskFragment(title: 'LongTask',),
      'count': (BuildContext context) => CounterFragment(title: 'CounterRX',),
    };
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                //color: Colors.lightGreenAccent,
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.lightGreenAccent,
                    Colors.white
                  ]
                ),
              ),
              child: Text(
                'Menu',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                ),
              ),
            ),
            setListItem(Icon(Icons.message), 'Messages', 'messages'),
            setListItem(Icon(Icons.call), 'Call', 'call'),
            setListItem(Icon(Icons.settings), 'Setting', 'setting'),
            setListItem(Icon(Icons.add), 'Canvas', 'canvas'),
            setListItem(Icon(Icons.router), 'Router', 'rout'),
            setListItem(Icon(Icons.integration_instructions_outlined), 'Intent', 'intent'),
            setListItem(Icon(Icons.data_usage_outlined), 'JSON', 'json'),
            setListItem(Icon(Icons.receipt_long_outlined), 'LongTask', 'longtask'),
            setListItem(Icon(Icons.countertops_outlined), 'CounterRX', 'count'),
            Divider(
              color: Colors.black,
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Exit'),
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have click the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              'Thenx for click',
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.plus_one),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget setListItem(Icon icon, String title, String way) {
    return ListTile(
      leading: icon,
      title: Text(title),
      tileColor: Colors.white,
      onTap: () {
        Navigator.of(context).pushNamed(way);
      },
    );
  }
}
