import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class IntentFragment extends StatefulWidget {
  IntentFragment({Key key, this.title}) : super(key: key);

  String title;

  @override
  State<StatefulWidget> createState() => _IntentPage();
}

class _IntentPage extends State<IntentFragment> {

  static const platform = MethodChannel('app.channel.shared.data');
  String dataShared = 'No data';

  @override
  void initState() {
    super.initState();
    getSharedText();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Text(dataShared),
      ),
    );
  }

  void getSharedText() async {
    var sharedText = await platform.invokeMethod('getSharedText');
    if (sharedText != null) {
      setState(() {
        dataShared = sharedText;
      });
    }
  }
}