import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:english_words/english_words.dart';

import 'RandomWords.dart';

class CallFragment extends StatefulWidget {
  CallFragment({Key key, this.title}) : super(key: key);

  String title;

  @override
  State<StatefulWidget> createState() => _CallFragmentPage();
}

class _CallFragmentPage extends State<CallFragment> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final _saved = <WordPair>{};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Startup Name Generator'),
        actions: [
          Stack(
            children: <Widget>[
              IconButton(icon: Icon(Icons.list_outlined), onPressed: _pushSaved),
              Positioned(
                top: 24.0,
                right: 8.0,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white
                  ),
                  height: 18.0,
                  width: 18.0,
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.green
                    ),
                    height: 17.0,
                    width: 17.0,
                    alignment: Alignment.center,
                    child: Text(_saved.length.toString(),
                        textAlign: TextAlign.center),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => RandomWords(saved: _saved)));
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, i) {
          if (i.isOdd) return const Divider();
          /*2*/

          final index = i ~/ 2; /*3*/
          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10)); /*4*/
          }
          return _buildRow(_suggestions[index]);
        });
  }

  Widget _buildRow(WordPair pair) {
    final alreadySaved = _saved.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        _updateLike(alreadySaved, pair);
      },
    );
  }

  void _updateLike(bool alreadySaved, WordPair pair) {
    setState(() {
      if (alreadySaved) {
        _saved.remove(pair);
      } else {
        _saved.add(pair);
      }
    });
  }
}
